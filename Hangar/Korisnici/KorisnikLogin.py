from Korisnici.Korisnik import Korisnik

class KorisnikLogin(Korisnik):

    def __init__(self, id= "", ime="", prezime="", korisnicko_ime="", lozinka=""):
        Korisnik.__init__(self, id, ime, prezime)

        self.korisnicko_ime = korisnicko_ime
        self.lozinka = lozinka

    def exportString(self):
        str = self.id + "|"
        str += self.ime + "|"
        str += self.prezime + "|"
        str += self.korisnicko_ime + "|"
        str += self.lozinka

        return str