from Korisnici.Korisnik import Korisnik


class Potrazitelj(Korisnik):


    def __init__(self, id="", ime="", prezime="", email= "", brojTelefona=""):
        Korisnik.__init__(self, id, ime, prezime)
        self.email = email
        self.brojTelefona = brojTelefona

    def exportString(self):
        str = self.id.__str__() + "|"
        str += self.ime + "|"
        str += self.prezime + "|"
        str += self.email + "|"
        str += self.brojTelefona

        return str
