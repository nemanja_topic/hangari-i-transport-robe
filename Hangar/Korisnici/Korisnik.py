class Korisnik(object):
    """
    Apstraktna klasa koja opisuje korisnika
    """
    def __init__(self, id="0", ime="", prezime=""):
        self.id = id
        self.ime = ime
        self.prezime = prezime

    def __str__(self, *args, **kwargs):
        return self.ime + " " + self.prezime