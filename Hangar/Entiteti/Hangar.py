class Hangar(object):

    def __init__(self, oznaka= "",naziv= "", duzina=0, sirina=0, visina=0, avioni=[]):
        self.oznaka = oznaka
        self.naziv = naziv
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.avioni = avioni

    def slobodnoDuzina(self):
        duzinaSlobodno = float(self.duzina)

        for avion in self.avioni:
            duzinaSlobodno = duzinaSlobodno - float(getattr(avion,"duzina"))

        return duzinaSlobodno

    def slobodnoSirina(self):
        sirinaSlobodno = float(self.sirina)

        for avion in self.avioni:
            sirinaSlobodno = sirinaSlobodno - float(getattr(avion,"rasponKrila"))

        return sirinaSlobodno

    def __str__(self):
        str = "Oznaka: " + self.oznaka + "\n"
        str += "Naziv: " + self.naziv + "\n"
        str += "Duzina: " + self.duzina.__str__() + "\n"
        str += "Sirina: " + self.sirina.__str__() + "\n"
        str += "Visina: " + self.visina.__str__() + "\n"
        str += "Avioni: \n"
        for avion in self.avioni:
            str += avion.__str__()

        return str

    def exportString(self):
        str = self.oznaka + "|"
        str += self.naziv + "|"
        str += str(self.duzina) + "|"
        str += str(self.sirina) + "|"
        str += str(self.visina) + "|"
        for avion in self.avioni:
            str += getattr(avion, "oznaka") + ";"

        str  = str[:-1]

        return str

