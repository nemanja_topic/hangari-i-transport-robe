from datetime import datetime
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport
class ZahtevZaTransport(object):

    def __init__(self,id= "",datumKreiranjaZahteva= datetime.now(), datumTransportaRobe= datetime.now (), odredisteTransporta="",
                 potrazitelj = None, avion = None, statusZahteva= StatusZahtevaZaTransport.kreiran, roba = {}):
        self.id = id
        self.datumKreiranjaZahteva = datumKreiranjaZahteva
        self.datumTransportaRobe = datumTransportaRobe
        self.odredisteTransporta = odredisteTransporta
        self.potrazitelj = potrazitelj
        self.avion = avion
        self.statusZahteva = statusZahteva
        self.roba = roba;

    def __str__(self):
        str = str = "id: " + self.id + "\n"
        str += "Datum kreiranja zahteva: " + self.datumKreiranjaZahteva.__str__() + "\n"
        str += "Datum transporta robe: " + self.datumTransportaRobe.__str__() + "\n"
        str += "Odredište transporta: " + self.odredisteTransporta + "\n"
        str += "Potražitelj: " + self.potrazitelj.__str__() + "\n"
        str += "Avion: " + self.avion.__str__() + "\n"
        str += "Status zahteva: " + self.statusZahteva.__str__() + "\n"
        str += "Roba: "
        for key in self.roba:
            str += key.__str__() + "\n"
        return str

    def exportString(self):
        str = self.id + "|"
        str += self.datumKreiranjaZahteva.__str__() + "|"
        str += self.datumTransportaRobe.__str__() + "|"
        str += self.odredisteTransporta + "|"
        str += getattr(self.potrazitelj, "id") + "|"
        str += getattr(self.avion, "oznaka") + "|"
        str += self.statusZahteva + "|"
        for r in self.roba.keys():
            str += getattr(r,"oznaka") + ";" + self.roba[r]

        return str
