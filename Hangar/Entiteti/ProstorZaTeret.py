class ProstorZaTeret(object):

    def __init__(self,naziv= "", duzina="", sirina="", visina="", roba= [] ):
        self.naziv = naziv
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.roba = roba

    def ubaci(self, rob):
        if float(getattr(rob, "visina")) < self.slobodnoVisina() and float(getattr(rob, "sirina")) < self.slobodnoSirina() and float(getattr(rob, "duzina")) < self.slobodnoDuzina():
            self.roba.append(rob)
            return True
        else:
            return False

    def slobodnoDuzina(self):
        duzinaSlobodno = float(self.duzina)

        for rob in self.roba:
            duzinaSlobodno = duzinaSlobodno - float(getattr(rob,"duzina"))

        return duzinaSlobodno

    def slobodnoSirina(self):
        sirinaSlobodno = float(self.sirina)

        for rob in self.roba:
            sirinaSlobodno = sirinaSlobodno - float(getattr(rob,"sirina"))

        return sirinaSlobodno

    def slobodnoDuzina(self):
        duzinaSlobodno = float(self.duzina)

        for rob in self.roba:
            duzinaSlobodno = duzinaSlobodno - float(getattr(rob,"duzina"))

        return duzinaSlobodno

    def slobodnoVisina(self):
        visinaSlobodno = float(self.visina)

        for rob in self.roba:
            visinaSlobodno = visinaSlobodno - float(getattr(rob,"visina"))

        return visinaSlobodno



    def __str__(self):
        str = "Naziv: " + self.naziv + "\n"
        str += "Dužina: " + self.duzina + "\n"
        str += "Širina: " + self.sirina + "\n"
        str += "Visina: " + self.visina + "\n"
        for r in self.roba:
            str += r.__str__()
        return str


    def exportString(self):
        str = self.naziv + "|"
        str += self.duzina + "|"
        str += self.sirina + "|"
        str += self.visina + "|"
        for rob in self.roba:
            str += getattr(rob, "oznaka") + ";"

        str  = str[:-1]

        return str
