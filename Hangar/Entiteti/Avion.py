class Avion(object):

    def __init__(self,oznaka= "",naziv= "", godiste="", duzina="", visina="", rasponKrila="",
                 nosivost="", relacija= [], uHangaru = False, prostorZaTeret = []):
        self.oznaka = oznaka
        self.naziv = naziv
        self.godiste = godiste
        self.duzina = duzina
        self.visina = visina
        self.rasponKrila = rasponKrila
        self.nosivost = nosivost
        self.relacija = relacija
        self.uHangaru = uHangaru
        self.prostorZaTeret = prostorZaTeret

    def __str__(self):
        str = "Oznaka: " + self.oznaka + "\n"
        str += "Naziv: " + self.naziv + "\n"
        str += "Godište: " + self.godiste + "\n"
        str += "Dužina: " + self.duzina + "\n"
        str += "Visina: " + self.visina + "\n"
        str += "Raspon krila: " + self.rasponKrila + "\n"
        str += "Nosivost: " + self.nosivost + "\n"
        str += "Relacije:\n"
        for rel in self.relacija:
            str += rel + ", "
        if self.uHangaru:
            str += "U hangaru: tacno\n"
        else:
            str += "U hangaru: netacno\n"
        for prostor in self.prostorZaTeret:
            str += prostor.__str__()
        return  str

    def exportString(self):
        str = self.oznaka + "|"
        str += self.naziv + "|"
        str += self.godiste + "|"
        str += self.duzina + "|"
        str += self.visina + "|"
        str += self.rasponKrila + "|"
        str += self.nosivost + "|"
        for rel in self.relacija:
            str += rel + ";"

        str  = str[:-1]
        str += "|"
        if self.uHangaru:
            str += "1"
        else:
            str += "0"
        str += "|"

        for prostor in self.prostorZaTeret:
            str += getattr(prostor, "naziv") + ";"

        str = str[:-1]

        return str
