class ZahtevZaSmestanjeAviona(object):

    def __init__(self,id= "",datumKreiranjaZahteva= "", vremeSmestanjaAviona="", vremeNapustanjaAviona="",
                 oznakaHangara="", avion= None, menadzer=None ):
        self.id = id
        self.datumKreiranjaZahteva = datumKreiranjaZahteva
        self.vremeSmestanjaAviona = vremeSmestanjaAviona
        self.vremeNapustanjaAviona = vremeNapustanjaAviona
        self.oznakaHangara = oznakaHangara
        self.avion = avion
        self.menadzer = menadzer

    def __str__(self):
        str = "ID: " + self.id + "\n"
        str += "Datum kreiranja zahteva: " + self.datumKreiranjaZahteva + "\n"
        str += "Vreme smeštanja aviona: " + self.vremeSmestanjaAviona.__str__() + "\n"
        str += "Vreme napuštanja aviona: " + self.vremeNapustanjaAviona.__str__() + "\n"
        str += "Oznaka hangara: \n" + self.oznakaHangara.__str__() + "\n"
        str += "Avion: \n" + self.avion.__str__() + "\n"
        str += "Menadžer: " + self.menadzer.__str__() + "\n"

        return str

    def exportString(self):
        str = self.id + "|"
        str += self.datumKreiranjaZahteva.__str__() + "|"
        str += self.vremeSmestanjaAviona + "|"
        str += self.vremeNapustanjaAviona + "|"
        str += str(self.oznakaHangara) + "|"
        str += getattr(self.avion, "oznaka") + "|"
        str += getattr(self.menadzer, "id") + "|"

        return str


