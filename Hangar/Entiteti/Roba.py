class Roba(object):

    def __init__(self,oznaka= "",naziv= "", opis="", duzina="", sirina="", visina="",
                 tezinaRobe="", idPotrazitelja= ""):
        self.oznaka = oznaka
        self.naziv = naziv
        self.opis = opis
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.tezinaRobe = tezinaRobe
        self.idPotrazitelja = idPotrazitelja

    def __str__(self):
        str = "Oznaka: " + self.oznaka + "\n"
        str += "Naziv: " + self.naziv + "\n"
        str += "Opis: " + self.opis + "\n"
        str += "Duzina: " + self.duzina + "\n"
        str += "Sirina: " + self.sirina + "\n"
        str += "Visina: " + self.visina + "\n"
        str += "Tezina robe: " + self.tezinaRobe + "\n"
        str += "ID potrazitelja: " + self.idPotrazitelja + "\n"

        return  str

    def exportString(self):
        str = self.oznaka + "|"
        str += self.naziv + "|"
        str += self.opis + "|"
        str += self.duzina + "|"
        str += self.sirina + "|"
        str += self.visina + "|"
        str += self.tezinaRobe + "|"
        for id in self.idPotrazitelja:
            str += getattr(id, "id") + ";"

        str  = str[:-1]

        return str