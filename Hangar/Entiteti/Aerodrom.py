class Aerodrom(object):

    def __init__(self,naziv= "", adresa="", mesto="", hangari = [] ):
        self.hangari = hangari
        self.naziv = naziv
        self.adresa = adresa
        self.mesto = mesto

    def __str__(self):
        str = "Naziv: " + self.naziv + "\n"
        str += "Adresa: " + self.adresa.__str__() + "\n"
        str += "Mesto: " + self.mesto.__str__() + "\n"
        str += "Hangari: \n"
        for hangar in self.hangari:
            str += hangar.__str__

        return str

    def exportString(self):
        str = self.naziv + "|"
        str += self.adresa + "|"
        str += self.mesto + "|"
        for hangar in self.hangari:
            str += getattr(hangar, "oznaka") + ";"

        str  = str[:-1]

        return str