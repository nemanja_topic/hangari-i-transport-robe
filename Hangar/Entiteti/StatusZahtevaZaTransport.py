from enum import Enum

class StatusZahtevaZaTransport(Enum):
    kreiran = 1,
    odobren = 2,
    utovarena = 3,
    transportovana = 4