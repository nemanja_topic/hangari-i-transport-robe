from IO import Import
from Functions import menadzerHangaraFunctions
from Functions import menadzerTransportaFunctions
from Functions import radnikFunctions
from Functions import potraziteljFunctions
from Korisnici.Potrazitelj import Potrazitelj
from IO import  Export


def ispisiPocetniMeni():

    print("*** Dobrodošli na aerodrom ***")
    print("1. Uloguj se")
    print("2. Registruj se")
    print("3. Uloguj se sa id-jem")
    print("4. Izadji")


def menadzerHangaraMeni():
    print("--- Menazder hangara meni ---\n"
          "\n"
          "1.Pogledaj zahteve za smeštanje aviona u hangar\n"
          "2.Pogledaj zahteve za transport robe koja je utovarena\n"
          "3.Dodaj nove hangare\n"
          "4.Dodaj nove avione\n"
          "5.Kreiraj zahtev za smeštanje aviona u hangar\n"
          "6.Vidi hangare\n"
          "7.Vidi avione\n"
          "8.Vidi prostore za teret i robu u prostoru za teret\n"
          "9.Pretraži hangare\n"
          "10.Pretraži avione\n"
          "11.Pretraži robu\n")


def menadzerTransportaMeni():
    print("--- Menazder transporta meni --- \n"
          "\n"
          "1.Vidi zahteve za transport robe\n"
          "2.Vidi zahteve za smeštanje aviona u hangar\n"
          "3.Odobri zahtev za transport robe\n"
          "4.Sortiraj zahteve za transport robe\n"
          "5.Vidi hangare\n"
          "6.Vidi avione\n"
          "7.Vidi prostore za teret i robu u prostoru za teret\n"
          "8.Pretraži hangare\n"
          "9.Pretraži avione\n"
          "10.Pretraži robu\n")


def radnikMeni():
    print("--- Radnik meni ---\n"
          "\n"
          "1.Pogledaj zahteve za transport robe koji su odobreni\n"
          "2.Unesi i rasporedi predmete iz zahteva za transport\n"
          "3.Vidi hangare\n"
          "4.Vidi avione\n"
          "5.Vidi prostore za teret i robu u prostoru za teret\n"
          "6.Pretraži hangare\n"
          "7.Pretraži avione\n"
          "8.Pretraži robu\n")


def potraziteljMeni():
    print("--- Potrazitelj meni ---\n"
          "1.Usluga transporta robe\n"
          "2.Vidi sve transporte robe koje si kreirao\n")


def logovanje(hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe):
    print("Unesite tip korisnika: ")
    print("1. Menadzer transporta")
    print("2. Menadzer hangara")
    print("3. Radnik")
    print()

    tip = 0

    while tip < 1 or tip > 3:
        tip = int(input("Unesite tip korisnika: "))

    korisnickoIme = input("Unesite korisnicko ime: ")
    lozinka = input("Unesite lozinku: ")

    if tip == 1:
        menadzeriTransporta = Import.importMenadzerTransporta()
        for menadzerTransporta in menadzeriTransporta:
            if getattr(menadzerTransporta, "korisnicko_ime") == korisnickoIme and getattr(menadzerTransporta, "lozinka") == lozinka:
                opcija = 1
                while opcija > 0 and opcija < 11:
                    menadzerTransportaMeni()
                    opcija = int(input("Unesite opciju: "))
                    menadzerTransportaFunctions.izvrsiOpciju(opcija, zahtevZaTransport, zahtevZaSmestanje, hangari, avioni, prostorZaTeret, robe)
            else:
                print("Neuspesno logovanje!")
    elif tip == 2:
        menadzeriHangara = Import.importMenadzerHangara()
        for menadzer in menadzeriHangara:
            if getattr(menadzer, "korisnicko_ime") == korisnickoIme and getattr(menadzer, "lozinka") == lozinka:
                opcija = 1
                while opcija > 0 and opcija < 13:
                    menadzerHangaraMeni()
                    opcija = int(input("Unesite opciju: "))
                    menadzerHangaraFunctions.izvrsiOpciju(opcija, hangari, avioni, prostorZaTeret, zahtevZaTransport, zahtevZaSmestanje, robe)
            else:
                print("Neuspesno logovanje!")
    elif tip == 3:
        radnici = Import.importRadnik()
        for radnik in radnici:
            if getattr(radnik, "korisnicko_ime") == korisnickoIme and getattr(radnik, "lozinka") == lozinka:
                opcija = 1
                while opcija > 0 and opcija < 10:
                    radnikMeni()
                    opcija = int(input("Unesite opciju: "))
                    radnikFunctions.izvrsiOpciju(opcija, zahtevZaTransport, hangari, avioni, prostorZaTeret, robe)
            else:
                print("Neuspesno logovanje!")


def nadjiId(potrazitelji):
    if potrazitelji.__len__() == 0:
        maxId = 0
    else:
        maxId = int(getattr(potrazitelji[0], "id"))

    for potrazitelj in potrazitelji:
        id = int(getattr(potrazitelj, "id"))
        if id > maxId:
            maxId = id

    return maxId + 1


def registracija(potrazitelji):
    id = nadjiId(potrazitelji)
    ime = input("Unesite ime: ")
    prezime = input("Unesite prezime: ")
    brojTelefona = input("Unesite broj telefona: ")
    email = input("Unesite email: ")

    potrazitelj = Potrazitelj(id, ime, prezime, brojTelefona, email)
    potrazitelji.append(potrazitelj)
    print("Vas id je: " + str(id))


def obradiOpcijuPotrazitelj(opcija, ulogovaniPotrazitelj, zahtevZaTransport, robe):
    if opcija == 1:
        potraziteljFunctions.zatraziTransport(zahtevZaTransport, robe, ulogovaniPotrazitelj)
        return
    elif opcija == 2:
        potraziteljFunctions.pregledajSveZahteve(zahtevZaTransport, ulogovaniPotrazitelj)


def logovanjePotrazitelj(potrazitelji, hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe):
    id = input("Unesite id: ")
    ulogovan = False
    ulogovaniPotrazitelj = None
    for potrazitelj in potrazitelji:
        if int(getattr(potrazitelj, "id")) == int(id):
            ulogovan = True
            ulogovaniPotrazitelj = potrazitelj
            break

    if ulogovan == False:
        print("Id ne postoji!")
    else:
        opcija = 1
        while opcija > 0 and opcija < 3:
            potraziteljMeni()
            opcija = int(input("Unesite opciju: "))
            obradiOpcijuPotrazitelj(opcija, ulogovaniPotrazitelj, zahtevZaTransport, robe)


def obradaOpcijePocetnogMenija(opcija, potrazitelji, hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe):
    if opcija == 1:
        logovanje(hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe)
    elif opcija == 2:
        registracija(potrazitelji)
    elif opcija == 3:
        logovanjePotrazitelj(potrazitelji, hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe)


def main():

    opcija = 1
    potrazitelji = Import.importPotrazitelj()

    hangari = Import.importHangar()
    avioni = Import.importAvion()
    prostorZaTeret = Import.importProstorZaTeret()
    zahtevZaTransport = Import.importZahtevZaTransport()
    prostor = Import.importProstorZaTeret()
    zahtevZaSmestanje = Import.importZahtevZaSmestanjeAviona()
    robe = Import.importRoba()

    while opcija > 0 and opcija < 4:
        ispisiPocetniMeni()
        opcija = int(input("Unesite opciju: "))
        obradaOpcijePocetnogMenija(opcija, potrazitelji, hangari, avioni, prostorZaTeret, zahtevZaTransport, prostor, zahtevZaSmestanje, robe)

    Export.exportAvion(avioni)
    Export.exportHangar(hangari)
    Export.exportPotrazitelj(potrazitelji)
    Export.exportZahtevZaSmestanjeAviona(zahtevZaSmestanje)
    Export.exportZahtevZaTransport(zahtevZaTransport)
    Export.exportRoba(robe)
    Export.exportProstorZaTeret(prostor)


main()
