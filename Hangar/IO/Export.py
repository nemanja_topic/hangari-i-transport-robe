from Korisnici.MenadzerHangara import MenadzerHangara
from Korisnici.MenadzerTransporta import MenadzerTransporta
from Korisnici.Radnik import Radnik
from Korisnici.Potrazitelj import Potrazitelj
from Entiteti.Aerodrom import Aerodrom
from Entiteti.Avion import Avion
from Entiteti.Hangar import Hangar
from Entiteti.ProstorZaTeret import ProstorZaTeret
from Entiteti.Roba import Roba
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport
from Entiteti.ZahtevZaSmestanjeAviona import ZahtevZaSmestanjeAviona
from Entiteti.ZahtevZaTransport import ZahtevZaTransport

def exportMenadzerHangara(menadzeri):
    with open("menadzeriHangara.txt", "w") as f:
        for menadzer in menadzeri:
            f.write(menadzer.exportString())

def exportMenadzerTransporta(menadzeri):
    with open("menadzeriTransporta.txt", "w") as f:
        for menadzer in menadzeri:
            f.write(menadzer.exportString())

def exportRadnik(radnici):
    with open("radnici.txt", "w") as f:
        for radnik in radnici:
            f.write(radnik.exportString())

def exportPotrazitelj(potrazitelji):
    with open("potrazitelj.txt", "w") as f:
        for potrazitelj in potrazitelji:
            f.write(potrazitelj.exportString())

def exportAerodrom(aerodromi):
    with open("aerodromi.txt", "w") as f:
        for aerodrom in aerodromi:
            f.write(aerodrom.exportString())

def exportHangar(hangari):
    with open("hangari.txt", "w") as f:
        for hangar in hangari:
            f.write(hangar.exportString())

def exportAvion(avioni):
    with open("avion.txt", "w") as f:
        for avion in avioni:
            f.write(avion.exportString())

def exportProstorZaTeret(prostorZaTeret):
    with open("prostor.txt", "w") as f:
        for prostor in prostorZaTeret:
            f.write(prostor.exportString())

def exportRoba(roba):
    with open("roba.txt", "w") as f:
        for rob in roba:
            f.write(rob.exportString())

def exportZahtevZaSmestanjeAviona(zahtevZaSmestanje):
    with open("zahtevzasmestanje.txt", "w") as f:
        for zahtev in zahtevZaSmestanje:
            f.write(zahtev.exportString())

def exportZahtevZaTransport(zahtevZaTransport):
    with open("zahtevzatransport.txt", "w") as f:
        for zahtev in zahtevZaTransport:
            f.write(zahtev.exportString())





