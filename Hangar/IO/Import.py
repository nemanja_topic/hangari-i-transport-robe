from Korisnici.MenadzerHangara import MenadzerHangara
from Korisnici.MenadzerTransporta import MenadzerTransporta
from Korisnici.Radnik import Radnik
from Korisnici.Potrazitelj import Potrazitelj
from Entiteti.Aerodrom import Aerodrom
from Entiteti.Avion import Avion
from Entiteti.Hangar import Hangar
from Entiteti.ProstorZaTeret import ProstorZaTeret
from Entiteti.Roba import Roba
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport
from Entiteti.ZahtevZaSmestanjeAviona import ZahtevZaSmestanjeAviona
from Entiteti.ZahtevZaTransport import ZahtevZaTransport

def importMenadzerHangara():
    menadzeriHangara = []
    with open("menadzeriHangara.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            menadzer = MenadzerHangara( podaci[0], podaci[1], podaci[2], podaci[3], podaci[4])
            menadzeriHangara.append(menadzer)
    return menadzeriHangara

def importMenadzerTransporta():
    menadzeriTransporta = []
    with open("menadzeriTransporta.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            menadzer = MenadzerTransporta(podaci[0], podaci[1], podaci[2], podaci[3], podaci[4])
            menadzeriTransporta.append(menadzer)
    return menadzeriTransporta

def importRadnik():
    radnici = []
    with open("radnici.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            radnik = Radnik(podaci[0], podaci[1], podaci[2], podaci[3], podaci[4])
            radnici.append(radnik)
    return radnici

def importPotrazitelj():
    potrazitelji = []
    with open("potrazitelj.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            potrazitelj = Potrazitelj(podaci[0], podaci[1], podaci[2], podaci[3], podaci[4])
            potrazitelji.append(potrazitelj)
    return potrazitelji


def importAerodrom():
    aerodromi = []
    hangari = importHangar()
    hangariNaAerodromu = []

    with open("aerodromi.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            hangarIds = podaci[3].split(';')

            for hangar in hangari:
                if getattr(hangar, "oznaka") in hangarIds:
                    hangariNaAerodromu.append(hangar)

            aerodrom = Aerodrom(podaci[0], podaci[1], podaci[2], hangariNaAerodromu)
            aerodromi.append(aerodrom)

    return aerodromi


def importHangar():
    hangari = []
    avioni = importAvion()
    avioniUHangaru = []

    with open("hangari.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            avionIds = podaci[5].split(';')

            for avion in avioni:
                if getattr(avion, "oznaka") in avionIds:
                    avioniUHangaru.append(avion)

            hangar = Hangar(podaci[0],podaci[1], float(podaci[2]), float(podaci[3]), float(podaci[4]), avioniUHangaru)
            hangari.append(hangar)
    return hangari

def importAvion():
    avioni = []
    prostori = importProstorZaTeret()
    prostorUavionu = []

    with open("avion.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            avionIds = podaci[9].split(';')

            uHangaru = False

            if podaci[8] == 1:
                uHangaru = True


            for prostor in prostori:
                if getattr(prostor, "naziv") in avionIds:
                    prostorUavionu.append(prostor)

            avion=Avion(podaci[0],podaci[1], podaci[2], podaci[3], podaci[4], podaci[5], podaci[6], podaci[7].split(';'),
                        uHangaru, prostorUavionu)

            avioni.append(avion)

    return avioni


def importProstorZaTeret():
    prostori = []
    robe = importRoba()
    robaUprostoru = []

    with open("prostor.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci=line.split('|')
            robaIds = podaci[4].split(';')
            robaUprostoru = []

            for roba in robe:
                if getattr(roba,"oznaka") in robaIds:
                    robaUprostoru.append(roba)

            prostor = ProstorZaTeret(podaci[0], podaci[1], podaci[2], podaci[3], robaUprostoru)
            prostori.append(prostor)

    return prostori

def importRoba():
    robe = []
    potrazitelji = importPotrazitelj()
    potraziteljZaRobu = []

    with open("roba.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')

            roba = Roba(podaci[0], podaci[1], podaci[2], podaci[3], podaci[4],
                                      podaci[5], podaci[6], podaci[7])
            robe.append(roba)

    return robe

def importZahtevZaSmestanjeAviona():
    zahtevi = []
    menadzeri = importMenadzerHangara()
    zahtevMenadzera = MenadzerHangara()
    avioni = importAvion()
    avionSmestanje = Avion ()
    hangari = importHangar()
    hangarSmestanje = Hangar()


    with open("zahtevzasmestanje.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci = line.split('|')
            menadzerId = podaci[6]
            avionId = podaci [5]
            hangarId = podaci [4]
            for menadzerHangara in menadzeri:
                if getattr(menadzerHangara,"id") == menadzerId:
                    zahtevMenadzera = menadzerHangara

            for avion in avioni:
                if(getattr(avion, "oznaka")) == avionId:
                    avionSmestanje = avion

            for hangar in hangari:
                if getattr(hangar, "oznaka") == hangarId:
                    hangarSmestanje = hangar

            zahtev = ZahtevZaSmestanjeAviona(podaci[0], podaci[1], podaci[2], podaci[3], hangarSmestanje,
                                             avionSmestanje,zahtevMenadzera)
            zahtevi.append(zahtev)

    return zahtevi

def importZahtevZaTransport():
    zahtevi = []
    robe  = importRoba()
    potrazitelji= importPotrazitelj()
    avioni = importAvion()
    potraziteljZahteva = Potrazitelj()
    avionZahtev = Avion()
    robaZahtev = {}

    with open("zahtevzatransport.txt", "r") as f:
        for line in f.readlines():
            line = line[:-1]
            podaci=line.split('|')
            potraziteljId = podaci[4]
            oznakaAviona = podaci[5]

            for avion in avioni:
                if(getattr(avion, "oznaka")) == oznakaAviona:
                    avionZahtev = avion

            for potrazitelj in potrazitelji:
                if getattr(potrazitelj,"id") == potraziteljId:
                    potraziteljZahteva = potrazitelj

            robaIzFajla = podaci[7].split(';')

            for roba in robaIzFajla:
                id, kolicina = roba.split(',')

                for r in robe:
                    if getattr(r, "oznaka") == id:
                        robaZahtev[r] = kolicina

            zahtev = ZahtevZaTransport(podaci[0], podaci[1], podaci[2], podaci[3], potraziteljZahteva, avionZahtev, podaci[6],
                               robaZahtev)
            zahtevi.append(zahtev)
    return zahtevi

