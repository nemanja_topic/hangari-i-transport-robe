from Entiteti.ZahtevZaTransport import ZahtevZaTransport
from Entiteti.Hangar import Hangar
from Entiteti.Avion import Avion
from Entiteti.Roba import Roba
from Entiteti.ProstorZaTeret import ProstorZaTeret
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport

def VidiOdobreneZahteve(zahtevZaTransport):
    for zahtev in zahtevZaTransport:
        if getattr(zahtev, "statusZahteva") == "odobren":
            print(zahtev)


def vidiHangare(hangari):
    for hangar in hangari:
        print(hangar)

def vidiAvione(avioni):
    for avion in avioni:
        print(avion)

def vidiProstorZaTeret(prostorZaTeret):
    for prostor in prostorZaTeret:
        print(prostor)


def pretraziHangariMeni():
    print("Pretrazi hangare")
    print("1. Po oznaci")
    print("2. Po nazivu")
    print("3. Po sirini")
    print("4. Po duzini")
    print("5. Po visini")

def pretraziHangare(hangari):
    opcija = 1

    while opcija > 0 and opcija < 6:
        pretraziHangariMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for hangar in hangari:
                if getattr(hangar, "oznaka") == oznaka:
                    rezultat.append(hangar)
        if opcija == 2:
            naziv = input("Unesite naziv: ")
            for hangar in hangari:
                if getattr(hangar, "naziv") == naziv:
                    rezultat.append(hangar)
        if opcija == 3:
            sirina = float(input("Unesite širinu: "))
            for hangar in hangari:
                if getattr(hangar, "sirina") == sirina:
                    rezultat.append(hangar)
        if opcija == 4:
            duzina = float(input("Unesite dužinu: "))
            for hangar in hangari:
                if getattr(hangar, "duzina") == duzina:
                    rezultat.append(hangar)
        if opcija == 5:
            visina = float(input("Unesite visinu: "))
            for hangar in hangari:
                if getattr(hangar, "visina") == visina:
                    rezultat.append(hangar)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiHangare(rezultat)

def postojiOznaka(hangari, oznaka):
    for hangar in hangari:
        if getattr(hangar, "oznaka") == oznaka:
            return True

    return False

def postojiNaziv(hangari, naziv):
    for hangar in hangari:
        if getattr(hangar, "naziv") == naziv:
            return True

    return False

def postojiSirina(hangari, sirina):
    for hangar in hangari:
        if getattr(hangar, "sirina") == sirina:
            return True

    return False

def postojiDuzina(hangari, duzina):
    for hangar in hangari:
        if getattr(hangar, "duzina") == duzina:
            return True

    return False

def postojiVisina(hangari, visina):
    for hangar in hangari:
        if getattr(hangar, "visina") == visina:
            return True

    return False



def pretraziAvioniMeni():
    print("Pretrazi avione")
    print("1. Po oznaci")
    print("2. Po dužini")
    print("3. Po rasponu krila")
    print("4. Po nosivosti")
    print("5. Po relaciji")

def pretraziAvione(avioni):
    opcija = 1

    while opcija > 0 and opcija < 6:
        pretraziAvioniMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for avion in avioni:
                if getattr(avion, "oznaka") == oznaka:
                    rezultat.append(avion)
        if opcija == 2:
            duzina = input("Unesite dužinu: ")
            for avion in avioni:
                if getattr(avion, "duzina") == duzina:
                    rezultat.append(avion)
        if opcija == 3:
            rasponKrila = float(input("Unesite raspon krila: "))
            for avion in avioni:
                if float(getattr(avion, "rasponKrila")) == rasponKrila:
                    rezultat.append(avion)
        if opcija == 4:
            nosivost = float(input("Unesite nosivost: "))
            for avion in avioni:
                if float(getattr(avion, "nosivost")) == nosivost:
                    rezultat.append(avion)
        if opcija == 5:
            relacija = input("Unesite relaciju: ")
            for avion in avioni:
                for relacija in getattr(avion, "relacija"):
                    if relacija == relacija:
                        rezultat.append(avion)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiAvione(rezultat)

def postojiOznaka(avioni, oznaka):
    for avion in avioni:
        if getattr(avion, "oznaka") == oznaka:
            return True

    return False

def postojiDuzina(avioni, duzina):
    for avion in avioni:
        if getattr(avion, "duzina") == duzina:
            return True

    return False

def postojiRasponKrila(avioni, rasponKrila):
    for avion in avioni:
        if getattr(avion, "rasponKrila") == rasponKrila:
            return True

    return False

def postojiNosivost(avioni, nosivost):
    for avion in avioni:
        if getattr(avion, "nosivost") == nosivost:
            return True

    return False

def postojiRelacija(avioni, relacija):
    for avion in avioni:
        if getattr(avion, "relacija") == relacija:
            return True

    return False

def vidiRobu(robe):
    for roba in robe:
        print(roba)

def pretraziRobuMeni():
    print("Pretrazi robu")
    print("1. Po oznaci")
    print("2. Po nazivu")
    print("3. Po opisu")
    print("4. Po duzini")
    print("5. Po širini")
    print("6. Po visini")
    print("7. Po težini")
    print("8. Po identifikacionom kodu potražitelja")

def pretraziRobu(robe):
    opcija = 1

    while opcija > 0 and opcija < 9:
        pretraziRobuMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for roba in robe:
                if getattr(roba, "oznaka") == oznaka:
                    rezultat.append(roba)
        if opcija == 2:
            naziv = input("Unesite naziv: ")
            for roba in robe:
                if getattr(roba, "naziv") == naziv:
                    rezultat.append(roba)
        if opcija == 3:
            opis = input("Unesite opis: ")
            for roba in robe:
                if getattr(roba, "opis") == opis:
                    rezultat.append(roba)
        if opcija == 4:
            duzina = input("Unesite dužinu: ")
            for roba in robe:
                if getattr(roba, "duzina") == duzina:
                    rezultat.append(roba)
        if opcija == 5:
            sirina = input("Unesite širinu: ")
            for roba in robe:
                if getattr(roba, "sirina") == sirina:
                    rezultat.append(roba)
        if opcija == 6:
            visina = input("Unesite visinu: ")
            for roba in robe:
                if getattr(roba, "visina") == visina:
                    rezultat.append(roba)
        if opcija == 7:
            tezinaRobe = input("Unesite tezinuRobe: ")
            for roba in robe:
                if getattr(roba, "tezinaRobe") == tezinaRobe:
                    rezultat.append(roba)
        if opcija == 8:
            idPotrazitelja = input("Unesite ID potražitelja: ")
            for roba in robe:
                print(getattr(roba, "idPotrazitelja"))
                if getattr(roba, "idPotrazitelja") == idPotrazitelja:
                    rezultat.append(roba)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiRobu(rezultat)

def postojiOznaka(robe, oznaka):
    for roba in robe:
        if getattr(roba, "oznaka") == oznaka:
            return True

    return False

def postojiNaziv(robe, naziv):
    for roba in robe:
        if getattr(roba, "naziv") == naziv:
            return True

    return False

def postojiOpis(robe, opis):
    for roba in robe:
        if getattr(roba, "opis") == opis:
            return True

    return False

def postojiDuzina(robe, duzina):
    for roba in robe:
        if getattr(roba, "duzina") == duzina:
            return True

    return False

def postojiSirina(robe, sirina):
    for roba in robe:
        if getattr(roba, "sirina") == sirina:
            return True

    return False

def postojiVisina(robe, visina):
    for roba in robe:
        if getattr(roba, "visina") == visina:
            return True

    return False

def postojiTezinaRobe(robe, TezinaRobe):
    for roba in robe:
        if getattr(roba, "TezinaRobe") == TezinaRobe:
            return True

    return False

def postojiIdPotrazitelja(robe, idPotrazitelja):
    for roba in robe:
        if getattr(roba, "idPotrazitelja") == idPotrazitelja:
            return True

    return False

def brojRobe(roba):
    broj = 0;

    for key in roba:
        broj = broj + int(roba[key])

    return broj

def rasporediRobu(zahtevZaTransport):
    avion = getattr(zahtevZaTransport, "avion")
    prostoriZaTeret = getattr(avion, "prostorZaTeret")
    roba = getattr(zahtevZaTransport, "roba")
    ubacenaRoba = 0
    for key in roba:
        for i in range(int(roba[key])):
            for prostor in prostoriZaTeret:
                if prostor.ubaci(key):
                    ubacenaRoba = ubacenaRoba + 1
                    break

    if ubacenaRoba == brojRobe(roba):
        print("Sva roba je ubacena")
        setattr(zahtevZaTransport, "statusZahteva", StatusZahtevaZaTransport.utovarena)
    else:
        print("Nema dovoljno mesta za svu robu")


def obradiOdobreneZahteve(zahtevi):
    for zahtev in zahtevi:
        if getattr(zahtev, "statusZahteva") != "odobren":
            continue
        rasporediRobu(zahtev)

def izvrsiOpciju(opcija,zahtevZaTransport,avioni,hangari,robe,prostorZaTeret):
    if opcija == 1:
        VidiOdobreneZahteve(zahtevZaTransport)
        return
    elif opcija == 2:
        obradiOdobreneZahteve(zahtevZaTransport)
        return
    elif opcija == 3:
        vidiHangare(hangari)
        return
    elif opcija == 4:
        vidiAvione(avioni)
        return
    elif opcija == 5:
        vidiProstorZaTeret(prostorZaTeret)
        return
    elif opcija == 6:
        pretraziHangare(hangari)
        return
    elif opcija == 7:
        pretraziAvione(avioni)
        return
    elif opcija == 8:
        pretraziRobu(robe)
        return