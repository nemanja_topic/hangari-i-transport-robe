from Entiteti.Hangar import Hangar
from Entiteti.Avion import Avion
from Entiteti.ProstorZaTeret import ProstorZaTeret
from Entiteti.ZahtevZaTransport import ZahtevZaTransport
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport
from Entiteti.ZahtevZaSmestanjeAviona import ZahtevZaSmestanjeAviona
from Entiteti.Roba import Roba
from IO import Import


def vidiZahtevZaSmestanje(ZahtevZaSmestanje):
    for zahtev in ZahtevZaSmestanje:
        print(zahtev)

def vidiUtovarenuRobu(zahtevZaTransport):
    for zahtev in zahtevZaTransport:
        if getattr(zahtev, "statusZahteva") == "utovarena":
            print(zahtev)

def vidiHangare(hangari):
    for hangar in hangari:
        print(hangar)

def vidiAvione(avioni):
    for avion in avioni:
        print(avion)

def vidiProstorZaTeret(prostorZaTeret):
    for prostor in prostorZaTeret:
        print(prostor)

def pretraziHangariMeni():
    print("Pretrazi hangare")
    print("1. Po oznaci")
    print("2. Po nazivu")
    print("3. Po sirini")
    print("4. Po duzini")
    print("5. Po visini")

def pretraziHangare(hangari):
    opcija = 1

    while opcija > 0 and opcija < 6:
        pretraziHangariMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for hangar in hangari:
                if getattr(hangar, "oznaka") == oznaka:
                    rezultat.append(hangar)
        if opcija == 2:
            naziv = input("Unesite naziv: ")
            for hangar in hangari:
                if getattr(hangar, "naziv") == naziv:
                    rezultat.append(hangar)
        if opcija == 3:
            sirina = float(input("Unesite širinu: "))
            for hangar in hangari:
                if getattr(hangar, "sirina") == sirina:
                    rezultat.append(hangar)
        if opcija == 4:
            duzina = float(input("Unesite dužinu: "))
            for hangar in hangari:
                if getattr(hangar, "duzina") == duzina:
                    rezultat.append(hangar)
        if opcija == 5:
            visina = float(input("Unesite visinu: "))
            for hangar in hangari:
                if getattr(hangar, "visina") == visina:
                    rezultat.append(hangar)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiHangare(rezultat)

def postojiOznaka(hangari, oznaka):
    for hangar in hangari:
        if getattr(hangar, "oznaka") == oznaka:
            return True

    return False

def postojiNaziv(hangari, naziv):
    for hangar in hangari:
        if getattr(hangar, "naziv") == naziv:
            return True

    return False

def postojiSirina(hangari, sirina):
    for hangar in hangari:
        if getattr(hangar, "sirina") == sirina:
            return True

    return False

def postojiDuzina(hangari, duzina):
    for hangar in hangari:
        if getattr(hangar, "duzina") == duzina:
            return True

    return False

def postojiVisina(hangari, visina):
    for hangar in hangari:
        if getattr(hangar, "visina") == visina:
            return True

    return False

def dodajHangar(hangari):
    print("Dodaj novi hangar")
    oznaka = input("Oznaka: ")

    if postojiOznaka(hangari, oznaka):
        print("Oznaka postoji!")
        return

    naziv = input("Naziv: ")
    duzina = float(input("Duzina: "))
    sirina = float (input("Sirina: "))
    visina = float (input("Visina: "))

    hangar = Hangar(oznaka, naziv, duzina, sirina, visina, [])
    hangari.append(hangar)

def dodajAvion(avioni):
    print("Dodaj novi avion: ")
    oznaka = input("Oznaka: ")

    if postojiOznaka(avioni, oznaka):
        print("Oznaka postoji!")
        return
    naziv= str (input("Naziv: "))
    godiste = float(input("Godiste: "))
    duzina = float(input("Duzina: "))
    visina = float (input("Visina: "))
    rasponKrila = float (input("Raspon krila: "))
    nosivost = float (input("Nosivost: "))

    avion = Avion(oznaka,naziv, godiste, duzina, visina, rasponKrila,nosivost, [],  False, [])
    avioni.append(avion)

def nadjiAvion(id):
    avioni = Import.importAvion()

    for avion in avioni:
        if getattr(avion,"oznaka") == id:
            return avion

    return None

def nadjiHangar(id):
    hangari = Import.importHangar()

    for hangar in hangari:
        if getattr(hangar,"oznaka") == id:
            return hangar

    return None

def nadjiMenadzeraHangara(id):
    menadzeriHangara = Import.importMenadzerHangara()

    for menadzer in menadzeriHangara:
        if getattr(menadzer,"id") == id:
            return menadzer

    return None



def kreirajZahtev(zahtevZaSmestanje):
    print("Kreiraj novi zahtev za smeštanje avion: ")
    id = input("ID: ")

    if postojiID(zahtevZaSmestanje, id):
        print("ID postoji!")
        return
    datumKreiranjaZahteva= str (input("Datum kreiranja zahteva: "))
    vremeSmestanjaAviona = str(input("Vreme smeštanja aviona: "))
    vremeNapustanjaAviona = str(input("Vreme napuštanja aviona: "))
    oznakaHangara = input("Oznaka hangara: ")
    oznakaAviona = input("Oznaka aviona: ")
    idMenadzera = input("ID menadžera: ")

    avion = nadjiAvion(oznakaAviona)
    hangar = nadjiHangar(oznakaHangara)
    menadzer = nadjiMenadzeraHangara(idMenadzera)

    if menadzer == None:
        print("Ne postoji menadzer sa unetim ID-jem")
        return

    if hangar == None:
        print("Ne postoji hangar sa unetom oznakom\n")
        return

    if avion == None:
        print("Ne postoji avion sa unetom oznakom\n")
        return

    if hangar.slobodnoDuzina() < float(getattr(avion,"duzina")):
        print("Avion ne moze stati po duzini")
        return

    if hangar.slobodnoSirina() < float(getattr(avion,"rasponKrila")):
        print("Avion ne moze stati po sirini")
        return

    if float(getattr(hangar,"visina")) < float(getattr(avion,"visina")):
        print("Avion ne moze stati po visini")
        return



    zahtevZaSmestanjeAviona = ZahtevZaSmestanjeAviona(id,datumKreiranjaZahteva, vremeSmestanjaAviona, vremeNapustanjaAviona,
                 hangar, avion, menadzer)
    zahtevZaSmestanje.append(zahtevZaSmestanjeAviona)


def postojiID(zahteviZaSmestanje, id):
    for zahtevZaSmestanje in zahteviZaSmestanje:
        if getattr(zahtevZaSmestanje, "id") == id:
            return True

def pretraziAvioniMeni():
    print("Pretrazi avione")
    print("1. Po oznaci")
    print("2. Po dužini")
    print("3. Po rasponu krila")
    print("4. Po nosivosti")
    print("5. Po relaciji")

def pretraziAvione(avioni):
    opcija = 1

    while opcija > 0 and opcija < 6:
        pretraziAvioniMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for avion in avioni:
                if getattr(avion, "oznaka") == oznaka:
                    rezultat.append(avion)
        if opcija == 2:
            duzina = input("Unesite dužinu: ")
            for avion in avioni:
                if getattr(avion, "duzina") == duzina:
                    rezultat.append(avion)
        if opcija == 3:
            rasponKrila = float(input("Unesite raspon krila: "))
            for avion in avioni:
                if float(getattr(avion, "rasponKrila")) == rasponKrila:
                    rezultat.append(avion)
        if opcija == 4:
            nosivost = float(input("Unesite nosivost: "))
            for avion in avioni:
                if float(getattr(avion, "nosivost")) == nosivost:
                    rezultat.append(avion)
        if opcija == 5:
            relacija = input("Unesite relaciju: ")
            for avion in avioni:
                for relacija in getattr(avion, "relacija"):
                    if relacija == relacija:
                        rezultat.append(avion)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiAvione(rezultat)

def postojiOznaka(avioni, oznaka):
    for avion in avioni:
        if getattr(avion, "oznaka") == oznaka:
            return True

    return False

def postojiDuzina(avioni, duzina):
    for avion in avioni:
        if getattr(avion, "duzina") == duzina:
            return True

    return False

def postojiSirina(avioni, sirina):
    for avion in avioni:
        if getattr(avion, "sirina") == sirina:
            return True

    return False

def postojiRasponKrila(avioni, rasponKrila):
    for avion in avioni:
        if getattr(avion, "rasponKrila") == rasponKrila:
            return True

    return False

def postojiNosivost(avioni, nosivost):
    for avion in avioni:
        if getattr(avion, "nosivost") == nosivost:
            return True

    return False

def postojiRelacija(avioni, relacija):
    for avion in avioni:
        if getattr(avion, "relacija") == relacija:
            return True

    return False

def vidiRobu(robe):
    for roba in robe:
        print(roba)

def pretraziRobuMeni():
    print("Pretrazi robu")
    print("1. Po oznaci")
    print("2. Po nazivu")
    print("3. Po opisu")
    print("4. Po duzini")
    print("5. Po širini")
    print("6. Po visini")
    print("7. Po težini")
    print("8. Po identifikacionom kodu potražitelja")

def pretraziRobu(robe):
    opcija = 1

    while opcija > 0 and opcija < 9:
        pretraziRobuMeni()
        opcija = int(input("Opcija: "))

        rezultat = []

        if opcija == 1:
            oznaka = input("Unesite oznaku: ")
            for roba in robe:
                if getattr(roba, "oznaka") == oznaka:
                    rezultat.append(roba)
        if opcija == 2:
            naziv = input("Unesite naziv: ")
            for roba in robe:
                if getattr(roba, "naziv") == naziv:
                    rezultat.append(roba)
        if opcija == 3:
            opis = input("Unesite opis: ")
            for roba in robe:
                if getattr(roba, "opis") == opis:
                    rezultat.append(roba)
        if opcija == 4:
            duzina = input("Unesite dužinu: ")
            for roba in robe:
                if getattr(roba, "duzina") == duzina:
                    rezultat.append(roba)
        if opcija == 5:
            sirina = input("Unesite širinu: ")
            for roba in robe:
                if getattr(roba, "sirina") == sirina:
                    rezultat.append(roba)
        if opcija == 6:
            visina = input("Unesite visinu: ")
            for roba in robe:
                if getattr(roba, "visina") == visina:
                    rezultat.append(roba)
        if opcija == 7:
            tezinaRobe = input("Unesite tezinuRobe: ")
            for roba in robe:
                if getattr(roba, "tezinaRobe") == tezinaRobe:
                    rezultat.append(roba)
        if opcija == 8:
            idPotrazitelja = input("Unesite ID potražitelja: ")
            for roba in robe:
                print(getattr(roba, "idPotrazitelja"))
                if getattr(roba, "idPotrazitelja") == idPotrazitelja:
                    rezultat.append(roba)

        if rezultat.__len__() == 0:
            print("Nema rezultata pretrage!")
        else:
            vidiRobu(rezultat)

def postojiOznaka(robe, oznaka):
    for roba in robe:
        if getattr(roba, "oznaka") == oznaka:
            return True

    return False

def postojiNaziv(robe, naziv):
    for roba in robe:
        if getattr(roba, "naziv") == naziv:
            return True

    return False

def postojiOpis(robe, opis):
    for roba in robe:
        if getattr(roba, "opis") == opis:
            return True

    return False

def postojiDuzina(robe, duzina):
    for roba in robe:
        if getattr(roba, "duzina") == duzina:
            return True

    return False

def postojiSirina(robe, sirina):
    for roba in robe:
        if getattr(roba, "sirina") == sirina:
            return True

    return False

def postojiVisina(robe, visina):
    for roba in robe:
        if getattr(roba, "visina") == visina:
            return True

    return False

def postojiTezinaRobe(robe, TezinaRobe):
    for roba in robe:
        if getattr(roba, "TezinaRobe") == TezinaRobe:
            return True

    return False

def postojiIdPotrazitelja(robe, idPotrazitelja):
    for roba in robe:
        if getattr(roba, "idPotrazitelja") == idPotrazitelja:
            return True

    return False







def izvrsiOpciju(opcija, hangari, avioni, prostorZaTeret, zahtevZaTransport,zahtevZaSmestanje,robe):
    if opcija == 1:
        print(zahtevZaSmestanje)
        vidiZahtevZaSmestanje(zahtevZaSmestanje)
        return
    elif opcija == 2:
        vidiUtovarenuRobu(zahtevZaTransport)
        return
    elif opcija == 3:
        dodajHangar(hangari)
        return
    elif opcija == 4:
        dodajAvion(avioni)
        return
    elif opcija == 5:
        kreirajZahtev(zahtevZaSmestanje)
        return
    elif opcija == 6:
        vidiHangare(hangari)
    elif opcija == 7:
        vidiAvione(avioni)
        return
    elif opcija == 8:
        vidiProstorZaTeret(prostorZaTeret)
        return
    elif opcija == 9:
        pretraziHangare(hangari)
    elif opcija == 10:
        pretraziAvione(avioni)
        return
    elif opcija == 11:
        pretraziRobu(robe)
        return
