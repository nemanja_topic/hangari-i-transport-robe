from Entiteti.ZahtevZaTransport import ZahtevZaTransport
from Entiteti.ZahtevZaSmestanjeAviona import ZahtevZaSmestanjeAviona
from Entiteti.Hangar import Hangar
from Entiteti.Avion import Avion
from Entiteti.ProstorZaTeret import ProstorZaTeret
from Entiteti.Roba import Roba
from Entiteti.StatusZahtevaZaTransport import StatusZahtevaZaTransport
from datetime import datetime

def nadjiId(zahteviZaTransportRobe):
    if zahteviZaTransportRobe.__len__() == 0:
        maxId = 0
    else:
        maxId = int(getattr(zahteviZaTransportRobe[0], "id"))

    for zahtev in zahteviZaTransportRobe:
        id = int(getattr(zahtev, "id"))
        if id > maxId:
            maxId = id

    return maxId + 1

def zatraziTransport(zahteviZaTransportRobe, robe, potrazitelj):
    print("Kreiranje zahteva")

    id = nadjiId(zahteviZaTransportRobe)
    odrediste = input("Unesite odrediste: ")

    opcija = 1
    robaZaTransport = {}

    while opcija == 1:
        print("1. Unesite robu")
        print("2. Izlaz")
        opcija = int(input("Unesite opciju: "))

        if opcija == 1:
            print("Raspoloziva roba: ")
            for rob in robe:
                print(rob)

            oznakaRobe = input("Unesite oznaku robe: ")
            kolicina = input("Unesite kolicinu robe: ")

            rob = None

            for r in robe:
                if getattr(r, "oznaka") == oznakaRobe:
                    rob = r
                    break

            if rob == None:
                print("Pogresna oznaka")
                continue
            else:
                robaZaTransport[rob] = kolicina

    zahtev = ZahtevZaTransport(str(id), datetime.now(), datetime.now(), odrediste, potrazitelj, None, StatusZahtevaZaTransport.kreiran, robaZaTransport)
    zahteviZaTransportRobe.append(zahtev)

def pregledajSveZahteve(zahteviZaTransport, ulogovaniPotrazitelj):
    for zahtev in zahteviZaTransport:
        potrazitelj = getattr(zahtev, "potrazitelj")

        if getattr(potrazitelj, "id") == getattr(ulogovaniPotrazitelj, "id"):
            print(zahtev)
